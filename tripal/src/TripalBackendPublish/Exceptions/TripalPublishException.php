<?php

namespace Drupal\tripal\TripalBackendPublish\Exceptions;

/**
 * Exception thrown for any general errors during publishing of content.
 */
class TripalPublishException extends \RuntimeException {}
