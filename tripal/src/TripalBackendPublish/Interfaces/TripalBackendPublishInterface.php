<?php

namespace Drupal\tripal\TripalBackendPublish\Interfaces;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Storage Backend-specific publish plugin implementations.
 */
interface TripalBackendPublishInterface extends PluginInspectionInterface {

}
