
The SQL files in this directory are directly from the official Chado package:
https://github.com/GMOD/Chado/tree/1.4/chado/migrations

The represent small incremental changes to the schema known as migrations which
are typically applied via Flyway. However, Tripal has it's own mechanisms for
managing schema which is why they are duplicated in our repo.
